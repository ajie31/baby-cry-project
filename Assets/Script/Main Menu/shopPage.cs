﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class shopPage : MonoBehaviour
{
    public GameObject toggles;

    public Color coldef;
    
    private int indexItem;
    private Toggle toggle;
    
    public List<Sprite> HeadWare;
    public List<Sprite> Diapers;
    public List<Sprite> Gloves;

    public List<string> descHeadware;
    public List<string> descDiapers;
    public List<string> descGloves;

    public List<int> headwarePrice;
    public List<int> diapersPrice;
    public List<int> glovesPrice;
    
    public List<List<Sprite>>itemLists = new List<List<Sprite>>();
    public List<List<string>> descriptionLists = new List<List<string>>();
    public List<List<int>> priceLists  = new List<List<int>>();
    public Image itemImage;

    public TextMeshProUGUI priceTag;
    public TextMeshProUGUI descText;
    public TextMeshProUGUI buttonText;

    public MainMenu mainMenu;
    //private List<string> itemDescriptions;
    
    private void Start()
    {
        
        PlayerPrefs.SetInt("coins",1000);
        itemLists.Add(HeadWare);
        itemLists.Add(Diapers);
        itemLists.Add(Gloves);

        descriptionLists.Add(descHeadware);
        descriptionLists.Add(descDiapers);
        descriptionLists.Add(descGloves);
        
        priceLists.Add(headwarePrice);
        priceLists.Add(diapersPrice);
        priceLists.Add(glovesPrice);
        
        //itemImage.sprite = spriteList[0];
        //descText.text = "This helmet can protect from enemies";
    }

    public void item1(Toggle _toggle)
    {
        itemImage.sprite = itemLists[mainMenu.choose][0];
        descText.text = descriptionLists[mainMenu.choose][0];
        indexItem = 0;
        toggle = _toggle;
        //itemStatus = PlayerPrefs.GetInt("itemId" + mainMenu.choose + 0);
        if (PlayerPrefs.GetInt("itemId" + mainMenu.choose +""+ indexItem) == 1)
        {
            buttonText.text = "Equip";
            buttonText.GetComponent<RectTransform>().anchorMax = new Vector2(0.92f,0.825f);
            priceTag.transform.parent.gameObject.SetActive(false);
        }
        else
        {
            buttonText.text = "Buy";
            priceTag.text = "$"+priceLists[mainMenu.choose][indexItem];
        }

    }
    public void item2(Toggle _toggle)
    {
        itemImage.sprite = itemLists[mainMenu.choose][1];
        //itemImage.sprite = spriteList[1];
        descText.text =descriptionLists[mainMenu.choose][1];
        indexItem = 1;
        toggle = _toggle;
        if (PlayerPrefs.GetInt("itemId" + mainMenu.choose +""+ indexItem) == 1)
        {
            buttonText.text = "Equip";
            buttonText.GetComponent<RectTransform>().anchorMax = new Vector2(0.92f,0.825f);
            priceTag.transform.parent.gameObject.SetActive(false);
            //priceTag.text = "Sold";
        }
        else
        {
            buttonText.text = "Buy";
            priceTag.text = "$"+priceLists[mainMenu.choose][indexItem];
        }
    }
    public void item3(Toggle _toggle)
    {
        itemImage.sprite = itemLists[mainMenu.choose][2];
        //itemImage.sprite = spriteList[2];
        descText.text = descriptionLists[mainMenu.choose][2];
        indexItem = 2;
        toggle = _toggle;
        if (PlayerPrefs.GetInt("itemId" + mainMenu.choose +""+ indexItem) == 1)
        {
            buttonText.text = "Equip";
            buttonText.GetComponent<RectTransform>().anchorMax = new Vector2(0.92f,0.825f);
            priceTag.transform.parent.gameObject.SetActive(false);
        }
        else
        {
            buttonText.text = "Buy";
            priceTag.text = "$"+priceLists[mainMenu.choose][indexItem];
        }
    }
    
    
    public void showInitial(int choosit,Toggle _toggle)
    {
        int i = 0;
        //Toggle togless;
        
        if (PlayerPrefs.GetInt("itemId" + mainMenu.choose +""+ indexItem) == 1)
        {
            buttonText.text = "Equip";
            buttonText.GetComponent<RectTransform>().anchorMax = new Vector2(0.92f,0.825f);
            priceTag.transform.parent.gameObject.SetActive(false);
        }
        else
        {
            priceTag.text = "$"+priceLists[choosit][0];
        }
       
        itemImage.sprite = itemLists[choosit][0];
        descText.text = descriptionLists[choosit][0];
        toggle = _toggle;

        foreach (Transform togle in toggles.transform)
        {
          
            if (PlayerPrefs.GetInt("itemId" + choosit +""+ i) == 1)
            {
                
                Toggle togless = togle.GetComponentInChildren<Toggle>();
                ColorBlock cb = togless.colors;
                cb.normalColor = Color.cyan;
                cb.highlightedColor = Color.cyan;
                togless.colors = cb;
            }
            else
            {
                Toggle togless = togle.GetComponentInChildren<Toggle>();
                ColorBlock cb = togless.colors;
                cb.normalColor = coldef;
                cb.highlightedColor = coldef;
                togless.colors = cb;
            }

            i++;
        }
    }

    public void buyItem()
    {
        
        if (PlayerPrefs.GetInt("itemId" + mainMenu.choose +""+ indexItem) == 1)
        {
            Debug.Log("Equiped");
        }
        else
        {
            
            if ((PlayerPrefs.GetInt("coins") - priceLists[mainMenu.choose][indexItem]) >= 0)
            {
                int i = 0;
                PlayerPrefs.SetInt("itemId" + mainMenu.choose + "" + indexItem, 1);
                PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins") - priceLists[mainMenu.choose][indexItem]);
                priceTag.transform.parent.gameObject.SetActive(false);
                buttonText.text = "Equip";
                mainMenu.coinText.text = "Coins : " + PlayerPrefs.GetInt("coins");
                ColorBlock cb = toggle.colors;
                cb.normalColor = Color.cyan;
                cb.highlightedColor = Color.cyan;
                toggle.colors = cb;
               /* foreach (Transform togle in toggles.transform)
                {
          
                    if (PlayerPrefs.GetInt("itemId" + mainMenu.choose +""+ i) == 1)
                    {
                
                        Toggle togless = togle.GetComponentInChildren<Toggle>();
                        ColorBlock cb = togless.colors;
                        cb.normalColor = Color.cyan;
                        cb.highlightedColor = Color.cyan;
                        togless.colors = cb;
                    }

                    i++;
                }*/
            
            }

        }
    }
    
   
}
