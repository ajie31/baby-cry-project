﻿
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class MainMenu : MonoBehaviour
{
   // public Toggle toggle;
    public RectTransform shop;
    public Toggle initialT;
    private Color overlayCol;
    public GameObject escapeMenu;
    public CanvasGroup cg;
    public CanvasGroup overlayShop;
    public int choose;
    public shopPage sP;

    public TextMeshProUGUI coinText;



    public void loadGame()
    {
        SceneManager.LoadScene(1);
    }

    public void quitApps()
    {
        Application.Quit();
    }

    public void cancelQuit()
    {
        escapeMenu.SetActive(false);
    }

    public void showTheShop()
    {
        overlayShop.gameObject.SetActive(true);
        coinText.text = "Coins : " + PlayerPrefs.GetInt("coins");
        StartCoroutine(showOverlay(overlayShop, overlayShop.alpha, 1f,0.3f));
        //StartCoroutine(movePanel(650f, 0f,cg.alpha,1f,0.3f));

    }

    public void hideTheShop()
    {
        StartCoroutine(movePanel(0f, 650f,cg.alpha,0f,0.3f,-1));
        
        
    }

    public void hideOverlayShop()
    {
        StartCoroutine(showOverlay(overlayShop, overlayShop.alpha, 0f, 0.3f,1));
    }

    public void showHeadware()
    {
        //shop.gameObject.SetActive(true);
        cg.gameObject.SetActive(true);
        
        choose = 0;

        initialT.isOn = true;
        sP.showInitial(choose,initialT);
        //sP.itemImage.sprite = sP.itemLists[choose][0];
       // sP.descText.text = sP.descriptionLists[choose][0];
        StartCoroutine(movePanel(650f, 0f,cg.alpha,1f,0.3f));
       
    }
    public void showDiapers()
    {
        //shop.gameObject.SetActive(true);
        cg.gameObject.SetActive(true);
        choose = 1;
        
        initialT.isOn = true;
        sP.showInitial(choose,initialT);
       // sP.itemImage.sprite = sP.itemLists[choose][0];
       // sP.descText.text = sP.descriptionLists[choose][0];
        StartCoroutine(movePanel(650f, 0f,cg.alpha,1f,0.3f));
        
    }
    public void showGloves()
    {
        //shop.gameObject.SetActive(true);
        cg.gameObject.SetActive(true);
        choose = 2;

        initialT.isOn = true;
        sP.showInitial(choose,initialT);
       //sP.itemImage.sprite = sP.itemLists[choose][0];
       // sP.descText.text = sP.descriptionLists[choose][0];
        StartCoroutine(movePanel(650f, 0f,cg.alpha,1f,0.3f));
        
    }

    
    private void FixedUpdate()
    {
        if (SceneManager.GetActiveScene().buildIndex == 0 && Input.GetKeyDown(KeyCode.Escape)&&!overlayShop.gameObject.activeInHierarchy)
        {
            escapeMenu.SetActive(true);
        }
    }

    private IEnumerator movePanel(float start, float end,float startA, float endA, float lerpTime = 0.5f,int reverse = 1)
    {
        float _timeStartedLerping = Time.time;
        float timeSinceStarted = Time.time - _timeStartedLerping;
        float percentageComplete = timeSinceStarted / lerpTime;

        while (true)
        {
            timeSinceStarted = Time.time - _timeStartedLerping;
            percentageComplete = timeSinceStarted / lerpTime;
            
            float currentValue = Mathf.Lerp(start, end, percentageComplete);
            float currentValueA = Mathf.Lerp(startA, endA, percentageComplete);
            shop.offsetMax = new Vector2(currentValue,0);
            shop.offsetMin = new Vector2(currentValue,0);
            //overlayCol.a = currentValueA;
            cg.alpha = currentValueA;
            //overlayShop.color = overlayCol;
           // cg.alpha = currentValue;

            if (percentageComplete >= 1) break;

            yield return new WaitForFixedUpdate();
        }

        if (reverse == -1)
        {
            //shop.gameObject.SetActive(false);
            cg.gameObject.SetActive(false);
        }
    }

    private IEnumerator showOverlay(CanvasGroup cg1, float start, float end, float lerpTime = 1,int reverse = 0)
    {
        float _timeStartedLerping = Time.time;
        float timeSinceStarted = Time.time - _timeStartedLerping;
        float percentageComplete = timeSinceStarted / lerpTime;

        while (true)
        {
            timeSinceStarted = Time.time - _timeStartedLerping;
            percentageComplete = timeSinceStarted / lerpTime;

            float currentValue = Mathf.Lerp(start, end, percentageComplete);

            cg1.alpha = currentValue;

            if (percentageComplete >= 1) break;

            yield return new WaitForFixedUpdate();
        }

        if (reverse == 1)
        {
            cg1.gameObject.SetActive(false);
        }
    }

}
