﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class gameManager : MonoBehaviour
{
    public float delayHeal = 3;
    public float environmentSpeed = 5;
    public int coin = 0;
    public int killCount = 0;
    public int cookiesCount = 0;
    private int coinInText = 0;
    private float timeScore = 1f;
    private int score = 0;
    private bool pause = false;
   // [SerializeField] private playerCollision pc;
    [SerializeField] private RectTransform cryMeter;
    private bool endGame = true;
    private bool setEnd = false;
    public GameObject endMenu;
    public TextMeshProUGUI textCoin;
    public TextMeshProUGUI textScore;
    public TextMeshProUGUI textKill;
    public TextMeshProUGUI textCookies;
    public TextMeshProUGUI textTotalCoins;
    public TextMeshProUGUI textTotalScore;

    private void Start()
    {
        //textScore.text = "Score : ";
        textCoin.text = "Coin: " + coinInText.ToString() ;
    }

    private void FixedUpdate()
    {
        if (cryMeter.anchorMax.x <=0 && endGame)
        {
            List<GameObject> tilesScript = new List<GameObject>(GameObject.FindGameObjectsWithTag("tile"));
            tilesScript.AddRange(new List<GameObject>(GameObject.FindGameObjectsWithTag("obsBelow")));

            foreach (var tile in tilesScript)
            {
                tile.GetComponent<environmentMove>().enabled = false;
            }
            endMenu.SetActive(true);
            setEnd = true;
            endGame = false;

            StartCoroutine("countAll");
        }
        if (timeScore > 0 && !pause && !setEnd)
        {
            timeScore -= Time.deltaTime;
        }

        if (timeScore <= 0 && !pause && !setEnd)
        {
            increaseScore();
        }
        

        if (coin > coinInText)
        {
            coinInText += 1; 
            textCoin.text = "Coin: " + coinInText.ToString();
        }
        if (delayHeal >0 )
        {
            delayHeal -= Time.deltaTime;
        }

        if (delayHeal<=0 && cryMeter.anchorMax.x <= 0.99)
        {
            cryMeter.anchorMax += new Vector2(0.001f,0);
                
        }

    }

    public void retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }

    private void increaseScore()
    {
        score += 1;
        timeScore = 1;
        textScore.text = "" + score;
    }

    private IEnumerator countAll()
    {
        int countKill = 0;
        int countCookies = 0;
        int countCoint = 0;
        int countScore = 0;

        while (true)
        {
            if (countCookies <= cookiesCount)
            {
                countCookies += 1;
                textCookies.text = "Cookies : " + countCookies;
            }

            if (countKill <= killCount)
            {
                countKill += 1;
                textKill.text = "Kills : " + countKill;
            }

            if (countCoint < coin)
            {
                countCoint += 1;
                textTotalCoins.text = "Total Coins : " +countCoint;
            }

            if (countScore < score)
            {
                countScore += 1;
                textTotalScore.text = "Total Score : " + countScore;
            }

            if (countCookies == cookiesCount && countKill == killCount && countCoint <= coin)
            {
                break;
            }
            
            
            
            yield return new WaitForFixedUpdate();
            
            
        }

    }
}
