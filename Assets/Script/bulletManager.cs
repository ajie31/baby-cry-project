﻿
using System.Net.Mime;
using TMPro;
using UnityEngine;

public class bulletManager : MonoBehaviour
{

    public RectTransform coinInd;
    public gameManager gM;

    private void Start()
    {
        gM = Camera.main.GetComponent<gameManager>();
        coinInd = GameObject.FindGameObjectWithTag("coinCount").GetComponent<RectTransform>();
    }

    private void OnEnable()
    {
        Invoke("Destroy", 1.5f);
    }

    private void Destroy()
    {
        gameObject.SetActive(false);
        
    }

    private void OnDisable()
    {
        CancelInvoke();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Transform tempPrent;
        if (other.CompareTag("enemy"))
        {
            tempPrent = other.transform.parent;
            other.transform.parent.gameObject.SetActive(false);
            gM.coin += 10;
            gM.killCount += 1;
            setCoinTrue(tempPrent.transform);
            
        }

        if (other.CompareTag("enemy1"))
        {
            
            other.gameObject.SetActive(false); 
            gM.coin += 10;
            gM.killCount += 1;
            setCoinTrue(other.transform);
        }
    }

    private void setCoinTrue(Transform target)
    {
        coinInd.GetComponent<TextMeshPro>().text = "+10";
        coinInd.SetParent(target.parent);
        coinInd.position = target.position;
        Invoke("setCoinFalse",.5f);
    }

    private void setCoinFalse()
    {
        coinInd.SetParent(null);
        coinInd.position = new Vector3(-16,20);
    }
}
