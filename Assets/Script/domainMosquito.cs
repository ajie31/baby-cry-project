﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class domainMosquito : MonoBehaviour
{
    private mosquito msqt;

    private void Start()
    {
        msqt = transform.GetChild(0).GetComponent<mosquito>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("enter");
            msqt.target = other.transform.position;
            msqt.getPlayer = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("Exit");
            
            msqt.getPlayer = false;
        }
    }
}
