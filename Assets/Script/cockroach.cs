﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cockroach : MonoBehaviour
{
    private int direction;
    private bool wallHitR = false;

    private void OnTriggerEnter2D(Collider2D obj)
    {
        if (obj.gameObject.layer == 10)
        {
            wallHitR = true;
        }
    }

    private void FixedUpdate()
    {
        if (transform.localPosition.x > 6f || wallHitR)
        {
            direction = -1;
            
        }
                
        if (transform.localPosition.x < 1f)
        {
            wallHitR = false;
            direction = 1;
           
        }

        var movement = new Vector3(1f,0,0) * direction * 2.0f * Time.deltaTime;
        transform.Translate(movement);
    }
    
    
}
