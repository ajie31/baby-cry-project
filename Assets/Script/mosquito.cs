﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mosquito : MonoBehaviour
{
    public Vector3 target;
    public bool getPlayer = false;

    private int direction;
    // Update is called once per frame
    void FixedUpdate()
    {

        if (!getPlayer)
        {
            if (transform.localPosition.y < .3f)
            {
                transform.localPosition = Vector3.MoveTowards(transform.localPosition, new Vector3(0.4f,0.3f,0), 0.5f * Time.deltaTime);
            }
            else
            {
                if (transform.localPosition.x > 0.35f)
                {
                    direction = -1;
                    //transform.localPosition = Vector3.MoveTowards(transform.localPosition, new Vector3(-0.4f,0,0), 1f * Time.deltaTime);
                }
                
                else if (transform.localPosition.x < -0.35f)
                {
                    direction = 1;
                    //transform.localPosition = Vector3.MoveTowards(transform.localPosition, new Vector3(0.4f,0,0), 1f * Time.deltaTime);
                }

                var movement = new Vector3(1f,0,0) * direction * 2.0f * Time.deltaTime;
                transform.Translate(movement);
                //transform.localPosition = new Vector3(Mathf.PingPong(Time.time *.5f, 0.4f) -0.4f,transform.localPosition.y);
            }
            
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, target, 15 * Time.deltaTime);
        }
        
    }
}
