﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class playerCollision : MonoBehaviour
{
    public gameManager gM;
    public RectTransform coinInd;
    public RectTransform vomitMeter;
    public RectTransform cryMeter;

    private void OnTriggerEnter2D(Collider2D obj)
    {
        if (obj.gameObject.layer == 10)
        {
            cryMeter.anchorMax = new Vector2(0,0);
        }
        if (obj.CompareTag("cookie"))
        {
            obj.transform.parent = null;
            obj.gameObject.SetActive(false);
            gM.cookiesCount += 1;
            
            
           /* if (vomitMeter.anchorMax.x < 0.99f)
            {
                vomitMeter.anchorMax += new Vector2(0.1f,0);
            }
            else
            {*/
                coinInd.GetComponent<TextMeshPro>().text = "+1";
                coinInd.SetParent(obj.transform.parent);
                coinInd.position = obj.transform.position;
                Invoke("objFalse",.4f);
                gM.coin += 1;
                //Debug.Log(gM.coin);
            
        }

        if (obj.CompareTag("notInstant"))
        {
            if (cryMeter.anchorMax.x >0.24f)
            {
                gM.delayHeal = 3;
                cryMeter.anchorMax -= new Vector2(0.2f,0);
                obj.transform.parent = null;
                obj.gameObject.SetActive(false);
            }
            else
            {
                cryMeter.anchorMax = new Vector2(0,0);
                obj.transform.parent = null;
                obj.gameObject.SetActive(false);
                //endGame = true;
            }
        }
        
        if (obj.CompareTag("notInstantTop"))
        {
            if (cryMeter.anchorMax.x >0.24f) 
            {
                gM.delayHeal = 3;
                cryMeter.anchorMax -= new Vector2(0.2f,0);
            }
            else
            {
                cryMeter.anchorMax = new Vector2(0,0);
                //endGame = true;
            }
        }
        if (obj.CompareTag("enemy")||obj.CompareTag("enemy1"))
        {
            if (cryMeter.anchorMax.x >0.32f) 
            {
                gM.delayHeal = 3;
                cryMeter.anchorMax -= new Vector2(0.3f,0);
                obj.gameObject.SetActive(false);
            }
            else
            {
                cryMeter.anchorMax = new Vector2(0,0);
                obj.gameObject.SetActive(false);
                //endGame = true;
            }
        }
    }

    private void objFalse()
    {
        coinInd.SetParent(null);
        coinInd.position = new Vector3(-16,20);
    }
}
