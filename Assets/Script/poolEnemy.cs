﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class poolEnemy : MonoBehaviour
{
    public static poolEnemy current;
    public int enemyAmount;
    public List<GameObject> enemyPrefab;

    private List<List<GameObject>> pooledEnemy = new List<List<GameObject>>();
    // Start is called before the first frame update

    private void Awake()
    {
        current = this;
    }

    void Start()
    {
        pooledEnemy.Add( new List<GameObject>());
        
        for (int i = 0; i < enemyAmount; i++)
        {
            GameObject enemy = (GameObject) Instantiate(enemyPrefab[0]);
            enemy.SetActive(false);
            pooledEnemy[0].Add(enemy);
        }
        pooledEnemy.Add( new List<GameObject>());
        for (int i = 0; i < enemyAmount; i++)
        {
            GameObject enemy = (GameObject) Instantiate(enemyPrefab[1]);
            enemy.SetActive(false);
            pooledEnemy[1].Add(enemy);
        }
    }

    public GameObject getPooledEnemy()
    {
        for (int i = 0; i < pooledEnemy[0].Count; i++)
        {
            if (!pooledEnemy[0][i].activeInHierarchy)
            {
                return pooledEnemy[0][i];
            }
        }

        return null;
    }
    public GameObject getPooledEnemy1()
    {
        for (int i = 0; i < pooledEnemy[1].Count; i++)
        {
            if (!pooledEnemy[1][i].activeInHierarchy)
            {
                return pooledEnemy[1][i];
            }
        }

        return null;
    }
}
