﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class environmentMove : MonoBehaviour
{
    public gameManager GM;

    private spawnTile sT;
    //private TilemapRenderer rend;

    private void Start()
    {
        sT = GameObject.FindWithTag("spawner").GetComponent<spawnTile>();
        GM = Camera.main.GetComponent<gameManager>();
    }

    void FixedUpdate()
    {
        transform.Translate(Vector3.left * Time.deltaTime*GM.environmentSpeed);

        if (transform.position.x <= -25)
        {
            for(int i = transform.childCount -1;i>=0;i--)
            {
                if (transform.GetChild(i).CompareTag("cookie")||transform.GetChild(i).CompareTag("notInstant")||transform.GetChild(i).CompareTag("notInstantTop")
                    ||transform.GetChild(i).CompareTag("mosquito"))
                {
                    //transform.GetChild(i).parent = null;
                    transform.GetChild(i).gameObject.SetActive(false);
                }

            }
            gameObject.SetActive(false);
            sT.spawnIt();
        }
    }

    /*private void OnBecameInvisible()
    {
        
        for(int i = transform.childCount -1;i>=0;i--)
        {
            if (transform.GetChild(i).CompareTag("cookie"))
            {
                //transform.GetChild(i).parent = null;
                transform.GetChild(i).gameObject.SetActive(false);
            }

            else if (transform.GetChild(i).CompareTag("notInstant"))
            {
                transform.GetChild(i).gameObject.SetActive(false);
            }
            else if (transform.GetChild(i).CompareTag("notInstantTop"))
            {
                transform.GetChild(i).gameObject.SetActive(false);
            }
        }
        gameObject.SetActive(false);
        sT.spawnIt();
       
    }*/
    


}
