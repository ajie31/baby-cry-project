﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnTile : MonoBehaviour
{
    //public gameManager GM;
    private int restObs = 3;

    // Update is called once per frame


    
    public void spawnIt()
    {
        var random = Random.value;
        var randomObs = Random.value;

       // GameObject tilesSpn = poolObject.current.GetPooledTile();
        GameObject tilesSpn;
        if (random > .5f)
        {
            restObs -= 1;
            tilesSpn = poolObject.current.GetPooledTile();
            tilesSpn.transform.position = transform.position ;
            StartCoroutine(spawnCookies1(tilesSpn));
            if (randomObs >0.6f)
            {
                GameObject obsGround = pooledObstacle.current.GetPooledObs();
                obsGround.transform.SetParent(tilesSpn.transform);
                obsGround.transform.position = new Vector3(24.5f ,-1.5f,0);
                obsGround.SetActive(true);
            }

            tilesSpn.SetActive(true);
        }
        else if (random > .3f && random < .5f)
        {
            restObs -= 1;
            tilesSpn = poolObject.current.getPooledTile1();
            tilesSpn.transform.position = transform.position ;
            spawnEnemy(tilesSpn);
            StartCoroutine(spawnCookie(tilesSpn));
            tilesSpn.SetActive(true);
            
        }
        else if (random < .3f)
        {
            if (restObs <=0)
            {
                tilesSpn = poolObject.current.getPooledTile2();
                tilesSpn.transform.position = transform.position ;
                StartCoroutine(spawnCookies1(tilesSpn));
                restObs = 3;
                //set obstacle Top
                GameObject obsTop = pooledObstacle.current.GetPooledObsTop();
                obsTop.transform.SetParent(tilesSpn.transform);
                obsTop.transform.position = new Vector3(Random.Range(24.5f,28.5f),8,0);
                
                obsTop.SetActive(true);
                tilesSpn.SetActive(true);
            }
            else
            {
                restObs -= 1;
                tilesSpn = poolObject.current.GetPooledTile();
                tilesSpn.transform.position = transform.position ;
                StartCoroutine(spawnCookies1(tilesSpn));
                if (randomObs >0.6f)
                {
                    GameObject obsGround = pooledObstacle.current.GetPooledObs();
                    obsGround.transform.SetParent(tilesSpn.transform);
                    obsGround.transform.position = new Vector3(24.5f ,-1.5f,0);
                    obsGround.SetActive(true);
                }
                tilesSpn.SetActive(true);
            }

        }
        
       /* if (random >0.8f)
        {
            GameObject obsGround = poolObject.current.GetPooledObs();
            obsGround.transform.SetParent(tilesSpn.transform);
            obsGround.transform.position = new Vector3(14f ,-1.5f,0);
            obsGround.SetActive(true);
        }*/
       
       
    }
    

    public IEnumerator spawnCookie(GameObject parent)
    {
        float j = -1.5f;
        int i = 0;
        int k = 1;
       float gap = 0;
        
        while (true)
        {
     
            GameObject cookie = poolObject.current.GetPooledCookies();
            cookie.transform.SetParent(parent.transform);
            cookie.transform.position = new Vector3(25.5f+(i+gap) ,j+3,0);
            cookie.SetActive(true);

            if (j>=-0.5f )
            {
                break;
            }
            
            if (i == k)
            {
                gap += 0.3f;
                i = -1;
                k -= 1;
                j += 1;
            }

            i += 1;
            yield return new WaitForFixedUpdate();
        }

    }
    public IEnumerator spawnCookies1(GameObject parent)
    {
     
        int i = 0;
        while (true)
        {
     
            GameObject cookie = poolObject.current.GetPooledCookies();
            cookie.transform.SetParent(parent.transform);
            cookie.transform.position = new Vector3(25.5f+(i) ,-1.5f,0);
            cookie.SetActive(true);

            if (i>=2f )
            {
                break;
            }
            
           

            i += 1;
            yield return new WaitForFixedUpdate();
        }

    }

    private void spawnEnemy(GameObject parent)
    {
        int random = Random.Range(0, 2);

        if (random == 0)
        {
            GameObject enemy = poolEnemy.current.getPooledEnemy();
            enemy.transform.SetParent(parent.transform);
            enemy.transform.localPosition = new Vector3(3.5f,4f,0);
            enemy.SetActive(true);
        }
        else
        {
            GameObject enemy = poolEnemy.current.getPooledEnemy1();
            enemy.transform.SetParent(parent.transform);
            enemy.transform.localPosition = new Vector3(.9f,.5f,0);
            enemy.SetActive(true);
        }
        
        
    }

}
