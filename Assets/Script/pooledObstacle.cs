﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pooledObstacle : MonoBehaviour
{
    public static pooledObstacle current;
    public GameObject obsTopPrefab;
   
    public List<GameObject> obsPrefab; 
    public int amountObsTop;
    public int amountObs;
    //private List<GameObject> obstacle;
    private List<GameObject> obstacleTop;
    private List<List<GameObject>> PooledObs = new List<List<GameObject>>();

    private void Awake()
    {
        current = this;
    }

    void Start()
    {
        
        obstacleTop = new List<GameObject>();
        PooledObs.Add(new List<GameObject>());
        for (int n = 0; n < amountObs; n++)
        {
            GameObject obsP = (GameObject)Instantiate(obsPrefab[0]);
            obsP.SetActive(false);
            PooledObs[0].Add(obsP);
        }
        PooledObs.Add(new List<GameObject>());
        for (int n = 0; n < amountObs; n++)
        {
            GameObject obsP = (GameObject)Instantiate(obsPrefab[1]);
            obsP.SetActive(false);
            PooledObs[1].Add(obsP);
        }

        for (int i = 0; i < amountObsTop; i++)
        {
            GameObject obsTopP =  (GameObject)Instantiate(obsTopPrefab);
            obsTopP.SetActive(false);
            obstacleTop.Add(obsTopP);
        }
    }
    public GameObject GetPooledObs()
    {
        int random =  Random.Range(0,2);

        for (int l = 0; l < PooledObs[random].Count; l++)
        {
           if (!PooledObs[random][l].activeInHierarchy)
            {
                return PooledObs[random][l];
            }
        } 
        return null;
    }
    public GameObject GetPooledObsTop()
    {
        // var random = Random.value;

        for (int l = 0; l < obstacleTop.Count; l++)
        {
            if (!obstacleTop[l].activeInHierarchy )
            {
                return obstacleTop[l];
            }
        } 
        return null;
    }

}
