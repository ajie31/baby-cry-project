﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class headMove : MonoBehaviour 
{
   public Slider slide;
    public Image handle;
    public Image background;
    /*private Color colHandle;
    private Color colBack;
    private Color colHandle1;
    private Color colBack1;*/
    private bool isDrag = false;
    //public swipeGesture SG;
   // private Vector3 eulerAngle = new Vector3(0,0,0);
    private float valueSlide;

   /* private void Start()
    {
        colHandle = handle.color;
        colHandle.a = 0.4f;
        colHandle1 = handle.color;
        colHandle1.a = 0f;
        
        colBack = background.color;
        colBack.a = 0.4f;
        colBack1 = background.color;
        colBack1.a = 0f;
    }*/

    void FixedUpdate()
    {

        if (!isDrag && (int)slide.value > 0)
        {
          // Debug.Log("here");
            
            slide.value -= 1f;
            valueSlide = 0;
            Quaternion relativeMove = Quaternion.Euler(0,0,0); 
            transform.rotation = Quaternion.Slerp(transform.rotation, relativeMove,  Time.deltaTime * 20.0f);
            
        }
        if (!isDrag &&(int)slide.value < 0)
        {
            //Debug.Log("here");
            slide.value += 1f;
            valueSlide = 0;
            Quaternion relativeMove = Quaternion.Euler(0,0,0); 
            transform.rotation = Quaternion.Slerp(transform.rotation, relativeMove,  Time.deltaTime * 20.0f);
        }
        /*if (SG._swipeDown && transform.rotation.z > -0.6f)
        {
            Debug.Log("here");
            eulerAngle.z = -100f;
            Quaternion deltarotaion = Quaternion.Euler(eulerAngle * Time.deltaTime);
            transform.rotation = transform.rotation * deltarotaion;


        }
        if (SG._swipeUp && transform.rotation.z < 0.6f)
        {
            Debug.Log("here");
            eulerAngle.z = 100f;
            Quaternion deltarotaion = Quaternion.Euler(eulerAngle * Time.deltaTime);
            transform.rotation = transform.rotation * deltarotaion;


        }
        
        if (!SG._swipeDown && !SG._swipeUp && (int)(transform.rotation.z * 10) >0 )
        {
            eulerAngle.z = -100f;
            Quaternion deltarotaion = Quaternion.Euler(eulerAngle * Time.deltaTime);
            transform.rotation = transform.rotation * deltarotaion;
        }        
        if (!SG._swipeDown && !SG._swipeUp && (int)(transform.rotation.z * 10) <0 )
        {
            eulerAngle.z = 100f;
            Quaternion deltarotaion = Quaternion.Euler(eulerAngle * Time.deltaTime);
            transform.rotation = transform.rotation * deltarotaion;
        }*/
        //Vector3 relativeMove = new Vector3(0,0,valueSlide);
        if (isDrag)
        {
           // Touch touch = Input.GetTouch(0);
            //handle.color = colHandle;
            //background.color = colBack;
           // handle.position = touch.position;
            Quaternion relativeMove = Quaternion.Euler(0,0,valueSlide*10f); 
            transform.rotation = Quaternion.Slerp(transform.rotation, relativeMove,  Time.deltaTime * 25.0f);
        }
        
    }

    public void toucheDn()
    {
        //Debug.Log("ll");
        isDrag = true;
    }  
    public void toucheUp()
    {
        isDrag = false;

        //handle.color = colHandle1;
       // background.color = colBack1;
    }

    public void headMovement(float z)
    {
        
        valueSlide = z;
    }

   /* public void normalizeSlide()
    {
        Debug.Log(mainSlider.value);
    }*/
}
