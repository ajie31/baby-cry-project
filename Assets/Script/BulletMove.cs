﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour
{
    private GameObject Head;
    public bool spawn;

    private void Start()
    {
        Head = GameObject.Find("head");
    }

    void FixedUpdate()
    {
        GetComponent<Rigidbody2D>().velocity = Head.transform.right * 40f;  
    }



}
