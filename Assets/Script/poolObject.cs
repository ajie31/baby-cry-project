﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class poolObject : MonoBehaviour
{
    public static poolObject current;
    public GameObject grid;
    public GameObject pooledPrefab;
    public GameObject coockiesPref;
    
    public List<GameObject> tile;
    
    public int amountPoolded ;
    public int amountTiles ;
    public int amountCookies ;
    

    private List<GameObject> PooledPrefabs;
    private List<GameObject> cookies;
    
    private List<List<GameObject>> PooledEnvironment = new List<List<GameObject>>();
   // private int restObs = 3;

    private void Awake()
    {
        Application.targetFrameRate = 30;
        current = this;
    }

    private void Start()
    {
        cookies = new List<GameObject>();
        PooledPrefabs = new List<GameObject>();
        PooledEnvironment.Add(new List<GameObject>()); 
        
        for (int i = 0; i < amountPoolded; i++)
        {
            GameObject projectile = (GameObject)Instantiate(pooledPrefab);
            projectile.SetActive(false);
            PooledPrefabs.Add(projectile);
        }  
        PooledEnvironment.Add(new List<GameObject>());
        for (int q = 0; q < amountTiles; q++)
        {
            
            //int randomPref = Random.Range(0, 2);
            GameObject tilesP1 = (GameObject)Instantiate(tile[0]);
            tilesP1.SetActive(false);
            tilesP1.transform.SetParent(grid.transform);
            PooledEnvironment[0].Add(tilesP1);
            
        }       
        PooledEnvironment.Add(new List<GameObject>());
        for (int w = 0; w < amountTiles; w++)
        {
            
           
            GameObject tilesP2 = (GameObject)Instantiate(tile[1]);
            tilesP2.SetActive(false);
            tilesP2.transform.SetParent(grid.transform);
            PooledEnvironment[1].Add(tilesP2);
            
        }
        for (int e = 0; e < amountTiles; e++)
        {
            
            
            GameObject tilesP3 = (GameObject)Instantiate(tile[2]);
            tilesP3.SetActive(false);
            tilesP3.transform.SetParent(grid.transform);
            PooledEnvironment[2].Add(tilesP3);
            
        }
        for (int k = 0; k < amountCookies; k++)
        {
            
            //int randomPref = Random.Range(0, 2);
            GameObject cookiesP = (GameObject)Instantiate(coockiesPref);
            cookiesP.SetActive(false);
            //tilesP.transform.SetParent(grid.transform);
            cookies.Add(cookiesP);
            
        }

        
    }

    public GameObject GetPooledObject()
    {
        for (int i = 0; i < PooledPrefabs.Count; i++)
        {
            if (!PooledPrefabs[i].activeInHierarchy)
            {
                return PooledPrefabs[i];
            }
        }

        return null;
    }
    public GameObject GetPooledTile()
    {
       // float rand = Random.value;

       for (int i = 0; i < PooledEnvironment[0].Count; i++)
       {
          if (!PooledEnvironment[0][i].activeInHierarchy)
          {
             //restObs -= 1;
             return PooledEnvironment[0][i];
          }
       }
        
        return null;  
    }

    public GameObject getPooledTile1()
    {
        
        for (int i = 0; i < PooledEnvironment[1].Count; i++)
        {
            if (!PooledEnvironment[1][i].activeInHierarchy)
            {
               // restObs -= 1;
                return PooledEnvironment[1][i];
            }
        }

        return null;
    }

    public GameObject getPooledTile2()
    {
         for (int i = 0; i < PooledEnvironment[2].Count; i++)
            {
                if (!PooledEnvironment[2][i].activeInHierarchy)
                {
                    //restObs = 3;
                    return PooledEnvironment[2][i];
                }
            }
        return null;
    }
    public GameObject GetPooledCookies()
    {
        for (int l = 0; l < cookies.Count; l++)
        {
            if (!cookies[l].activeInHierarchy)
            {
                return cookies[l];
            }
        }

        return null;
    }

   

}

/* else if (rand > .3f && rand < .5f)
       {
           for (int i = 0; i < PooledEnvironment[1].Count; i++)
           {
               if (!PooledEnvironment[1][i].activeInHierarchy)
               {
                   restObs -= 1;
                   return PooledEnvironment[1][i];
               }
           }
       }
       else if(rand < .3f)
       {
           if (restObs <= 0)
           {
               for (int i = 0; i < PooledEnvironment[2].Count; i++)
               {
                   if (!PooledEnvironment[2][i].activeInHierarchy)
                   {
                       restObs = 3;
                       return PooledEnvironment[2][i];
                   }
               }
           }
           else
           {
               for (int i = 0; i < PooledEnvironment[0].Count; i++)
               {
                   if (!PooledEnvironment[0][i].activeInHierarchy)
                   {
                       restObs -= 1;
                       return PooledEnvironment[0][i];
                   }
               }
           }

       }
       for (int i = 0; i < PooledEnvironment.Count; i++)
       {
           if (!PooledEnvironment[i].activeInHierarchy)
           {
               return PooledEnvironment[i];
           }
       }*/