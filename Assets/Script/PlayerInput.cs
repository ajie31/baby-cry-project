﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    
   // [SerializeField] private Slider slide;
   // [SerializeField] private LineRenderer lr;
    //[SerializeField] private RectTransform vomitMeter;
    private Rigidbody2D rb;
    public GameObject Head;
    //public gameManager GM;
    private float distanceGround;
    public Image ChannelBar;
    public RectTransform channelling;
    public float _channelValue = 0;
   // public RectTransform channeling;
    private float tempSpeed;
    //private Vector2 left = new Vector2(-30f,0f);
    public bool acc = false;
    public bool hitSome = false;
    private bool shoot = false;
    //private float channelTime = 0.5f;
    //private float holdFire = 0 ;
    
    
    // Start is called before the first frame update
    void Start()
    {
        distanceGround = GetComponent<Collider2D>().bounds.extents.y;
       // holdFire = channelTime;
        //channelling = ChannelBar.gameObject.transform.parent.gameObject;
        rb = GetComponent<Rigidbody2D>();
       
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 fwd = transform.TransformDirection (Vector3.down);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, fwd, 0.5f);
        if (hit.collider != null)
        {
            Debug.Log("false");
            acc = false;
        }
       /*
        if (acc && hitSome && slide.value <= 10f && slide.value >= -9f )
        {
            GM.environmentSpeed = 3 + (2* Mathf.Abs(slide.value/9f));
            rb.AddRelativeForce(new Vector2(0,5f+(20f * Mathf.Abs((slide.value/9f)))) * Time.deltaTime ,ForceMode2D.Impulse);
            
            
        }

        if (acc && hitSome && slide.value < -9f )
        {
            rb.AddRelativeForce(new Vector2(0,18f) * Time.deltaTime ,ForceMode2D.Impulse);
            GM.environmentSpeed = 7;
        }
        
        if (acc && !hitSome)
        {
            GM.environmentSpeed = tempSpeed;
        }*/

        if (transform.position.y > 2f)
        {
            rb.AddForce(Vector3.down *15);
           // hitSome = true;
            // Debug.Log("down");
        }
        
       /* if (acc && slide.value > -7f)
        {
            
            GM.environmentSpeed = 3;
        }*/
        

      /*  if (shoot && channeling.anchorMin.x < 1)
        {

            holdFire += Time.deltaTime;
            channeling.anchorMin = new Vector2(holdFire/channelTime,channeling.anchorMin.y);
            
        }*/
        
        if (shoot && _channelValue < 1.0f)
        {
            

            _channelValue += Time.deltaTime/0.5f;
            channelProgress(_channelValue);
            
            //channeling.anchorMin = new Vector2(holdFire/channelTime,channeling.anchorMin.y);
            
        }
        
    }

    private bool isGrounded()
    {
        return Physics2D.Raycast(transform.position, -Vector3.up, distanceGround + 0.1f);
    }

    private void channelProgress(float channelValue)
    {
        float amount = (channelValue / 1.0f) * 180.0f/360.0f;
        ChannelBar.fillAmount = amount;
    }
    public void Bttn1()
    {

            if (Input.touchCount == 1)
            {
                Touch touch = Input.GetTouch(0);
               channelling.position = touch.position + new Vector2(-100f,0f);

            }

            if (Input.touchCount ==2)
            {
                Touch touch = Input.GetTouch(1);
                channelling.position = touch.position + new Vector2(-100f,0f);
            }
            channelling.gameObject.SetActive(true);
            shoot = true;
            Debug.Log("button 1");
        

    }   
    
    public void Bttn1Up()
    {
       // if (channeling.anchorMin.x >=1)
        if (_channelValue >=1)
        {
            GameObject projectiles = poolObject.current.GetPooledObject();
            //vomitMeter.anchorMax += new Vector2(-0.05f,0);
            projectiles.transform.position =  Head.transform.position + Head.transform.right;
            projectiles.SetActive(true);
            
        }
       
        
        shoot = false;
        _channelValue = 0;
        channelProgress(_channelValue);
        //channeling.anchorMin = new Vector2(0,channeling.anchorMin.y);
        channelling.gameObject.SetActive(false);
       // Debug.Log("button 1 Up");
    }   
    public void Bttn2()
    {
       // tempSpeed = GM.environmentSpeed;
        if (!acc)
        {
            acc = true;
            rb.AddForce(Vector2.up * 600.0f); 
            
        }
       
       
        //lr.enabled = true;
        // rb.AddForce(new Vector2(0f,100f) * Time.deltaTime,ForceMode2D.Impulse);
       // Debug.Log("button 2");
    }     
    public void Bttn2Up()
    {
       // GM.environmentSpeed = tempSpeed;
        //acc = false;
        //lr.enabled = false;
       // hitSome = false;
        // rb.AddForce(new Vector2(0f,100f) * Time.deltaTime,ForceMode2D.Impulse);
        //Debug.Log("button 2Up");
    } 
    
    
    
}
